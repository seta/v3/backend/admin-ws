import time
import logging
import logging.config
import random
import string
import os
from fastapi import FastAPI, Request

from admin.internal import configuration

from admin.routers.indexes import router as indexes_router
from admin.routers.testing import router as testing_router

# setup loggers
logging.config.fileConfig("/etc/seta/logging.conf", disable_existing_loggers=False)
log_level = os.environ.get("LOG_LEVEL", default=logging.getLogger().getEffectiveLevel())

root_logger = logging.getLogger()
root_logger.setLevel(log_level)

admin_logger = logging.getLogger("admin")
admin_logger.setLevel(log_level)


def create_app() -> FastAPI:
    """Web service app factory"""

    app = FastAPI(
        root_path="/seta-admin", title="Internal Administration", version="0.0.1"
    )

    @app.middleware("http")
    async def log_requests(request: Request, call_next):
        """Logs the time every request takes."""

        idem = "".join(random.choices(string.ascii_uppercase + string.digits, k=6))
        admin_logger.info("rid=%s start request path=%s", idem, request.url.path)
        start_time = time.time()

        response = await call_next(request)

        process_time = (time.time() - start_time) * 1000
        formatted_process_time = f"{process_time:.2f}"
        admin_logger.info(
            "rid=%s completed_in=%sms status_code=%s",
            idem,
            formatted_process_time,
            response.status_code,
        )

        return response

    app.include_router(indexes_router, tags=["indexes"])

    if configuration.stage.lower() == "test":
        app.include_router(testing_router, tags=["testing"])

    root_logger.info("FastAPI initialized.")

    return app
