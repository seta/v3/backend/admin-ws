FROM python:3.10

RUN useradd seta 
ARG ROOT=/home/seta

WORKDIR $ROOT

# set env variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

#copy configuration files
COPY ./config/data.conf /etc/seta/
COPY ./config/logging.conf /etc/seta/

COPY ./admin $ROOT/admin
COPY ./*.py ./

CMD ["uvicorn", "--factory", "main:create_app", "--host=0.0.0.0", "--port=8000", "--no-access-log"]
